# NEWYORK CITY SCHOOLS APPLICATION

iOS Project for JPMorganChase.

### Project Overview
This application will take the user to look for the current List of School that exists in new York City; The application Offers the Following

 - Nice Looking UI
 - Home UI
 - List Of Schools
 - School Detail
 - School Favorites (Incomplete)

 ### Technical Details
The Application was created using the following technologies that iOS SDK & Swift Language Offers:

 - iOS v15.0 as Minimal version
 - `Swift Language`
 - `SwiftUI` as Declarative UI Programming Language
 - `Async/Await` for Api networking Calls (decided to not use combine due the conditions of the application which is not a big project).
 - `Unit Testing` - Offered just for the Authentication Layer implementing Mock Objects for Async Await Calls.
 - Set of Icons for multiple Sizes
 - App Icon for all the options available in Assets -> appIcon

 ### What I would have done if I had more time
 - I would have finish the Favorites Feature, that would contain the following:
    - Hability to See the Saved School that the user could potentialy like while checking Details and clicking on the Saved Button on TopRight Corner of    DetailSchool View
    - Hability to Remove School from Favorite List
    - Reusing the SchoolList View in order to avoid code duplication for the SchoolFavorites View
    - I would have Used Combine Whitin a StoreProvider in order to Reactively Save, Delete and Fetch the Schools from Persistence Store
 - iOS Code Base:
    - I would have used Custom propertywrappers/Modifiers to make my life easier in development
    - I Would have implemented an iPad version using ScreenSizes (.compact, regular, etc)
    - I Would have implemented The App Theme Style by creating a SwiftPackage and importing it and mantaining it as a separate dependency instead of relying on Structs.
    - I would have created a SwiftPackage for Core Functionality and Implemented it inside the project to make sure that could grow intoa. potential dependency for another projects
    - I would have Used Components Based Approach instead of Having ALl the Views inside the Code Base (let's call it Modularization).
 - CI/CD:
    - Gitlab:
        - I Would have set the CI/CD Pipeline for Linter, SonarQuality (Code Smells and Code Coverage since I have Unit Testing), Build and Tests  for PR Review, and For When Merged to Develop and Master.
        - I would have implemented Swift Linter and it's basic rules since this is a very small project.
    - Fastlane:
        - I would have Implemented the Fastlane to run Automatic test Cases for the PRReview Pipeline in Gitlab, Deploy to testflight and whatever lanes are needed for a small project to have Automation Done Easy.


That's pretty much It, I may be missing something but since is a very small project that's Enough.

Please reach out to me at: ing.gabrieljaramillo@gmail.com or my phone Number 404-484-4528
Please feel free to give a try into a project where I collaborated from scratch to completion for Rapid Backend Server Development called `Expressive-Tea` (https://www.expressive-tea.io).

Thank you a lot and it's a pleasure talking to you all.
