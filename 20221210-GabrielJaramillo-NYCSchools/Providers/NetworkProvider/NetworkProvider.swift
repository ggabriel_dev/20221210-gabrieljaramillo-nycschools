//
//  NetworkProvider.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import Foundation

final class NetworkProvider {
    private let session: URLSessionProtocol
    
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    func fetch<T: Decodable>(endPoint: APIService, model: T.Type) async throws -> Result<T, Error> {
        guard let endPoint = URL(string: endPoint.rawValue) else {
            return .failure(URLError(.badServerResponse))
        }
        
        do {
            let (data, _) = try await session.data(from: endPoint, delegate: nil)
            let users = try JSONDecoder().decode(model, from: data)
            return .success(users)
        } catch {
            return .failure(URLError(.badServerResponse))
        }
    }
}

protocol URLSessionProtocol {
    func data(from url: URL, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse)
}

extension URLSessionProtocol {
    func data(from url: URL, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        try await data(from: url, delegate: nil)
    }
}

extension URLSession: URLSessionProtocol { }
