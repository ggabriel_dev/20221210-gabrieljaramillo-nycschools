//
//  APIService.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import Foundation
enum APIService: String {
    case schools = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
}
