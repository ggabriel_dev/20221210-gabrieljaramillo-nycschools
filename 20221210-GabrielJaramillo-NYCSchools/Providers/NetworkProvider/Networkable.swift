//
//  Networkable.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/13/22.
//

import Foundation

protocol Networkable {
    func fetch<T: Decodable>(endPoint: APIService, model: T.Type) async throws -> Result<T, Error>
}
