//
//  _0221210_GabrielJaramillo_NYCSchoolsApp.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/10/22.
//

import SwiftUI

@main
struct _0221210_GabrielJaramillo_NYCSchoolsApp: App {
    
    init() {
        UINavigationBar.appearance().barTintColor = .clear
        UINavigationBar.appearance().tintColor = .clear
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().isOpaque = true
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        UITableView.appearance().backgroundColor = .clear
    }
    
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
