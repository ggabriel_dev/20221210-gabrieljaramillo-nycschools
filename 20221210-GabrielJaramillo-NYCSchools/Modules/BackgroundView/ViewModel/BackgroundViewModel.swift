//
//  BackgroundViewModel.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/10/22.
//

import Foundation
import Combine
class BackgroundViewModel: ObservableObject {
    @Published var backgroundTimer = Timer.publish(every: 6, on: .main, in: .common).autoconnect()
}
