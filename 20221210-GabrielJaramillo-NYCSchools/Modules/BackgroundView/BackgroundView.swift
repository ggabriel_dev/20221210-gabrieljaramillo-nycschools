//
//  BackgroundView.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/10/22.
//

import SwiftUI

struct BackgroundImage: View {
    @ObservedObject private var viewModel: BackgroundViewModel = BackgroundViewModel()
    @State var imageName: String = AppStyle.Images.defaultImageName
    @State var backgroundDidChange: Bool = true
    
    var imageSize: CGSize {
        UIImage(named: imageName)?.size ?? CGSize(width: 0, height: 0)
    }
    
    var body: some View {
        ZStack {
            Image(imageName)
                .resizable()
                .aspectRatio(imageSize, contentMode: .fill)
                .onReceive(viewModel.backgroundTimer) { _ in
                    guard let newImage = AppStyle.Images.backgroundImages.randomElement() else {
                        imageName = "ny-a"
                        backgroundDidChange = true
                        return
                    }
                    withAnimation(.easeOut.delay(0.2)) {
                        imageName = newImage
                    }
                }

            Color.black
                .opacity(backgroundDidChange ? 0.25 : 1)
                .onReceive(viewModel.backgroundTimer) { _ in
                    withAnimation(.easeIn
                        .speed(1)) {
                            backgroundDidChange.toggle()
                        }
                    
                    
                    withAnimation(.easeOut.speed(0.3).delay(0.8)) {
                        backgroundDidChange.toggle()
                    }

                }
        }
        .ignoresSafeArea()
    }
}
