//
//  HomeViewModel.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/11/22.
//

import Foundation

class HomeViewModel: ObservableObject {
    @Published var iconTimer = Timer.publish(every: AppStyle.Timming.halfSecond,
                                             on: .main,
                                             in: .common).autoconnect()
    @Published var backgroundImage: BackgroundImage = BackgroundImage()
    @Published var destination: SchoolList = SchoolList()
}
