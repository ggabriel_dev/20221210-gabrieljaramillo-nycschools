//
//  HomeView.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/10/22.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject private var viewModel: HomeViewModel = HomeViewModel()
    @State var showIcon: Bool = true

    var body: some View {
        NavigationView {
            content
                .background(viewModel.backgroundImage)
        }
    }
    
    var content: some View {
        ZStack {
            AppStyle.Colors.black
                .opacity(AppStyle.Home.Background.opacity)
                .ignoresSafeArea()
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .zIndex(AppStyle.ZIndexes.zero)
                .tag(AppStyle.Tags.zero)
            VStack {
                Spacer()
                topTitle
                bottomTitle
                startButton
                    .padding(.top)
                Spacer()
            }
            .foregroundColor(AppStyle.Colors.white)
            .zIndex(AppStyle.ZIndexes.one)
            .tag(AppStyle.Tags.one)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
    
    var topTitle: some View {
        HStack(spacing: .zero) {
            Text("""
                 \(AppStyle.Home.Titles.welcome)
                 """)
            .tag(AppStyle.Tags.zero)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.leading, AppStyle.Spacing.thirthy)
            .font(.custom(AppStyle.Fonts.alfaSlabOneRegular,
                          size: AppStyle.Home.Titles.welcomeSize))
            Image(AppStyle.Home.Images.hatx64)
                .renderingMode(.template)
                .foregroundColor(showIcon ? AppStyle.Colors.white : AppStyle.Colors.clear)
                .padding(.trailing, AppStyle.Spacing.thirthy)
                .onReceive(viewModel.iconTimer) { _ in
                    showIcon.toggle()
                }
                .tag(AppStyle.Tags.one)
        }
    }
    
    var bottomTitle: some View {
        HStack(spacing: .zero) {
            Text("""
                 \(AppStyle.Home.Titles.to)
                 """)
            .tag(AppStyle.Tags.zero)
            .frame(maxWidth: AppStyle.Spacing.twenty, alignment: .trailing)
            .padding(.leading, AppStyle.Spacing.thirthy)
            .font(.custom(AppStyle.Fonts.alfaSlabOneRegular,
                          size: AppStyle.Home.Titles.toSize))
            Text("""
                 \(AppStyle.Home.Titles.newYorkSchools)
                 """)
            .tag(AppStyle.Tags.one)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.leading, AppStyle.Spacing.fourthy)
            .font(.custom(AppStyle.Fonts.alfaSlabOneRegular,
                          size: AppStyle.Home.Titles.newYorkSchoolsSize))
        }
    }
    
    var startButton: some View {
        NavigationLink(AppStyle.Home.Titles.startSearching, destination: {
            viewModel.destination
        })
        .onTapGesture {
            viewModel.iconTimer.upstream.connect().cancel()
        }
        .font(.custom(AppStyle.Fonts.alfaSlabOneRegular,
                      size: AppStyle.Sizes.sixteen))
        .padding()
        .background(Capsule().fill(.purple))
        .background(Capsule()
            .fill(.purple.opacity(AppStyle.OpacityLevels.pointFour))
            .rotationEffect(.init(degrees: -AppStyle.Sizes.ten)))
        .background(Capsule()
            .fill(.purple.opacity(AppStyle.OpacityLevels.pointFour))
            .rotationEffect(.init(degrees: AppStyle.Sizes.six)))
        .background(Capsule()
            .fill(.purple.opacity(AppStyle.OpacityLevels.pointFour))
            .rotationEffect(.init(degrees: AppStyle.Sizes.ten)))
        .background(Capsule()
            .fill(.purple.opacity(AppStyle.OpacityLevels.pointFour))
            .rotationEffect(.init(degrees: -AppStyle.Sizes.six)))
        .shadow(color: AppStyle.Colors.teal,
                radius: AppStyle.Sizes.twenty,
                x: .zero,
                y: -AppStyle.Sizes.two)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .previewInterfaceOrientation(.portrait)
    }
}
