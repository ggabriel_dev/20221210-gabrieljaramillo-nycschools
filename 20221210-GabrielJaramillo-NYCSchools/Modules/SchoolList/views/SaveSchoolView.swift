//
//  SaveSchool.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import SwiftUI

struct SaveSchoolView: View {
    var body: some View {
        Text(Image(AppStyle.Icons.starx32))
            .onTapGesture {
                // save School in Favorites
            }
    }
}

struct SaveSchoolView_Previews: PreviewProvider {
    static var previews: some View {
        SaveSchoolView()
    }
}
