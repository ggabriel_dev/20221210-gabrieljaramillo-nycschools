//
//  BackButton.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import SwiftUI

struct BackButtonView: View {
    var body: some View {
        content
    }
    
    private var content: some View {
        HStack {

            Image(AppStyle.Icons.backx32)
                .resizable()
                .renderingMode(.template)
                .foregroundColor(AppStyle.Colors.white)
                .frame(width: AppStyle.Sizes.twentytwo,
                       height: AppStyle.Sizes.twentytwo)

//            Text(AppStyle.SchoolList.Navigation.)
//                .font(.custom(AppStyle.Fonts.staatlichesRegular,
//                              size: AppStyle.Sizes.sixteen))
//                .foregroundColor(AppStyle.Colors.white)

        }
        .padding([.all], AppStyle.Sizes.four)
        .background(
            RoundedRectangle(cornerRadius: AppStyle.Sizes.eight)
                .stroke(AppStyle.Colors.white,
                        lineWidth: AppStyle.Sizes.one)
                .foregroundColor(.clear)
        )
    }
}

struct BackButtonView_Previews: PreviewProvider {
    static var previews: some View {
        BackButtonView()
            .background(.black)
            .previewLayout(.sizeThatFits)
    }
}
