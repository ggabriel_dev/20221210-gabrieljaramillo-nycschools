//
//  StatisticsView.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import SwiftUI

struct StatisticsView: View {
    @State var school: School
    
    var statistics: [String] {
        school.buildStatistics()
    }
    
    var body: some View {
        VStack {
            
            title

            description

            ForEach(statistics, id: \.self) { value in
                HStack {
                    Image(AppStyle.Icons.checkmarkx16)
                        .resizable()
                        .renderingMode(.template)
                        .frame(width: AppStyle.Sizes.twentyfour,
                               height: AppStyle.Sizes.twentyfour,
                               alignment: .leading)
                    Text(value)
                        .font(.custom(AppStyle.Fonts.staatlichesRegular,
                                      size: AppStyle.Sizes.sixteen))
                }
                .frame(maxWidth: .infinity, alignment: .leading)
            }
        }
        .padding([.all],
                 AppStyle.Spacing.thirthy)
        .fixedSize(horizontal: false, vertical: true)
        .foregroundColor(AppStyle.Colors.white)
        .background(AppStyle.Colors.spaceBlack)

    }
    
    private var title: some View {
        HStack {
            Image(AppStyle.Icons.chartx16)
                .resizable()
                .renderingMode(.template)
                .frame(width: AppStyle.Sizes.thirtytwo,
                       height: AppStyle.Sizes.thirtytwo,
                       alignment: .leading)
            
            Text(AppStyle.SchoolDetail.Statistics.title)
                .font(.custom(AppStyle.Fonts.staatlichesRegular,
                              size: AppStyle.Sizes.thirtytwo))
                .frame(maxWidth: .infinity,
                       alignment: .leading)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    private var description: some View {
        Text(AppStyle.SchoolDetail.Statistics.description)
            .frame(maxWidth: .infinity,
                   alignment: .leading)
            .font(.custom(AppStyle.Fonts.staatlichesRegular,
                          size: AppStyle.Sizes.eighteen))
    }
}

struct StatisticsView_Previews: PreviewProvider {
    @State static var school = School()
    static var previews: some View {
        StatisticsView(school: school)
            .previewLayout(.sizeThatFits)
    }
}
