//
//  RoundedContainer.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import SwiftUI

struct SchoolTopContainerView: View {
    @State var school: School
    
    var schoolName: String {
        school.name ?? AppStyle.StringPrefixes.notAvailable
    }
    
    var campusName: String {
        school.campusName ?? AppStyle.StringPrefixes.notAvailable
    }
    
    var schoolAddress: String {
        school.buildSchoolAddress()
    }
    
    var busList: String {
        AppStyle.SchoolDetail.SchoolTopContainerView.busRoutes + school.buildBuses()
    }
    
    var websiteName: String {
        school.buildWebsite()
    }
    
    var emailAddress: String {
        school.buildEmail()
    }
    
    var phoneNumber: String {
        school.buildPhone()
    }
    
    var faxNumber: String {
        school.buildFax()
    }
    
    var directions: String {
        school.buildDirections()
    }
    
    var body: some View {
        VStack(spacing: 0) {
            schoolNameView
            campusNameView
            locationView
            descriptionView
            informationView
            busesView
            subwaysView
        }
        .padding([.all], 30)
        .fixedSize(horizontal: false, vertical: true)
        .foregroundColor(AppStyle.Colors.white)
        .background(AppStyle.Colors.spaceBlack)
    }
    private var schoolNameView: some View {
        Text(schoolName)
            .font(.custom(AppStyle.Fonts.staatlichesRegular,
                          size: 32))
            .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    private var campusNameView: some View {
        Text(campusName)
            .frame(maxWidth: .infinity, alignment: .leading)
            .font(.custom(AppStyle.Fonts.staatlichesRegular,
                          size: 18))
    }
    
    private var locationView: some View {
        InformationCell(iconName: AppStyle.Icons.locationv2x16,
                        title: schoolAddress,
                        alignment: .leading)
    }
    
    private var descriptionView: some View {
        VStack {
            Text(AppStyle.SchoolList.Titles.description)
                .fontWeight(.bold)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 4)
            Text(AppStyle.SchoolDetail.SchoolTopContainerView.directionsTitle)
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(.custom(AppStyle.Fonts.staatlichesRegular,
                              size: 18))
                .padding(.top, 1)
            Text(directions)
                .fontWeight(.bold)
                .frame(maxWidth: .infinity, alignment: .leading)
        }
        .font(.custom(AppStyle.Fonts.staatlichesRegular, size: AppStyle.Sizes.sixteen))
    }
    
    private var informationView: some View {
        VStack {
            Text(AppStyle.SchoolDetail.SchoolTopContainerView.informationTitle)
                .font(.custom(AppStyle.Fonts.staatlichesRegular,
                              size: 20))
                .frame(maxWidth: .infinity, alignment: .leading)
            HStack {
                
                VStack {
                    InformationCell(iconName: AppStyle.Icons.websitex16,
                                    title: websiteName,
                                    alignment: .center,
                                    hasDescription: false)
                    InformationCell(iconName: AppStyle.Icons.phonex32,
                                    title: phoneNumber,
                                    alignment: .center,
                                    hasDescription: false)
                }
                .padding([.bottom, .top], 8)
                
                VStack {
                    InformationCell(iconName: AppStyle.Icons.emailx32,
                                    title: emailAddress,
                                    alignment: .center,
                                    hasDescription: false)
                    InformationCell(iconName: AppStyle.Icons.fax2x32,
                                    title: faxNumber,
                                    alignment: .center,
                                    hasDescription: false)
                }
                .padding([.bottom, .top], 8)
                
            }
            .frame(maxWidth: .infinity, alignment: .leading)
        }
        .padding(.top, 20)
    }
    
    private var busesView: some View {
        VStack {
            Text(AppStyle.SchoolDetail.SchoolTopContainerView.transportation)
                .font(.custom(AppStyle.Fonts.staatlichesRegular,
                              size: 20))
                .frame(maxWidth: .infinity, alignment: .leading)
            HStack {
                Image(AppStyle.Icons.bus2x16)
                    .resizable()
                    .renderingMode(.template)
                    .frame(width: 24, height: 24, alignment: .leading)
                
                Text(busList)
                    .font(.custom(AppStyle.Fonts.staatlichesRegular, size: 14))
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top, 10)
        }
        .padding(.top, 20)
    }
    
    private var subwaysView: some View  {
        HStack {
            Image(AppStyle.Icons.subwayx16)
                .resizable()
                .renderingMode(.template)
                .frame(width: 24, height: 24, alignment: .leading)
            
            Text(busList)
                .font(.custom(AppStyle.Fonts.staatlichesRegular, size: 14))
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.top, 10)
    }
}

struct RoundedContainer_Previews: PreviewProvider {
    @State static var school: School = School()
    static var previews: some View {
        SchoolTopContainerView(school: school)
            .previewLayout(.sizeThatFits)
    }
}
