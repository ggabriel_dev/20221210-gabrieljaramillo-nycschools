//
//  InformationCell.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import SwiftUI

struct InformationCell: View {
    @State var iconName: String
    @State var title: String
    @State var alignment: Alignment
    @State var hasDescription: Bool = false
    @State var description: String?
    
    var descriptionContent: String {
        description ?? AppStyle.StringPrefixes.notAvailable
    }
    
    var body: some View {
        content
    }
    var informationTitle: some View {
        HStack {
            Image(iconName)
                .resizable()
                .renderingMode(.template)
                .frame(width: 24, height: 24, alignment: .leading)
            
            Text(title)
                .font(.custom(AppStyle.Fonts.staatlichesRegular, size: 14))
        }
        .frame(maxWidth: .infinity, alignment: alignment)
    }
    @ViewBuilder
    var content: some View {
        if !hasDescription {
            informationTitle
        } else {
            VStack {
                informationTitle
                Text(descriptionContent)
                    .font(.custom(AppStyle.Fonts.staatlichesRegular, size: 14))
            }
        }
    }
}

struct InformationCell_Previews: PreviewProvider {
    @State static var iconName: String = AppStyle.Icons.emailx32
    @State static var title: String = "Information Title"
    static var previews: some View {
        InformationCell(iconName: iconName,
                        title: title,
                        alignment: .leading,
                        hasDescription: false,
                        description: "")
        .previewLayout(.sizeThatFits)
    }
}
