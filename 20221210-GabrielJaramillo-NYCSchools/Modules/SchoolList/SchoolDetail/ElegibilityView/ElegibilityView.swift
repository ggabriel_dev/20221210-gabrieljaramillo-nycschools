//
//  ElegibilityView.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import SwiftUI

struct ElegibilityView: View {
    @State var school: School
    
    var elegibilityOverview: String {
        school.buildElegibility()
    }
    var body: some View {
        VStack {
            title
            
            description
            
            elegibility
        }
        .padding([.all],
                 AppStyle.Spacing.thirthy)
        .fixedSize(horizontal: false, vertical: true)
        .foregroundColor(AppStyle.Colors.white)
        .background(AppStyle.Colors.spaceBlack)
    }
    
    var title: some View {
        HStack {
            Image(AppStyle.Icons.checkmarkx16)
                .resizable()
                .renderingMode(.template)
                .frame(width: AppStyle.Sizes.thirtytwo,
                       height: AppStyle.Sizes.thirtytwo,
                       alignment: .leading)
            
            Text(AppStyle.SchoolDetail.Elegibility.title)
                .font(.custom(AppStyle.Fonts.staatlichesRegular,
                              size: AppStyle.Sizes.thirtytwo))
                .frame(maxWidth: .infinity,
                       alignment: .leading)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    var description: some View {
        Text(AppStyle.SchoolDetail.Elegibility.description)
            .frame(maxWidth: .infinity,
                   alignment: .leading)
            .font(.custom(AppStyle.Fonts.staatlichesRegular,
                          size: AppStyle.Sizes.eighteen))
    }
    
    var elegibility: some View {
        Text(elegibilityOverview)
            .frame(maxWidth: .infinity,
                    alignment: .leading)
            .font(.custom(AppStyle.Fonts.staatlichesRegular, size: AppStyle.Sizes.sixteen))
            .padding(.top, AppStyle.Spacing.one)
    }
}

struct ElegibilityView_Previews: PreviewProvider {
    @State static var school: School = School()
    static var previews: some View {
        ElegibilityView(school: school)
    }
}
