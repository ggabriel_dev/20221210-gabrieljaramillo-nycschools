//
//  SchoolDetail.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import SwiftUI

struct SchoolDetailView: View {
    @Environment(\.presentationMode) var presentation
    @ObservedObject private var viewModel: SchoolDetailViewModel = SchoolDetailViewModel()
    @State var school: School
    
    var schoolName: String {
        school.name ?? AppStyle.StringPrefixes.notAvailable
    }
    
    var body: some View {
        NavigationView {
            content
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    private var divider: some View {
        Divider()
            .background(.white)
    }
    var content: some View {
        ZStack {
            
            ScrollView(showsIndicators: false) {
                topSection
                
                divider
                
                statisticsView
                
                divider
                
                elegibilityView
                
                divider
            }
            .zIndex(.zero)
            .padding(.top, AppStyle.Sizes.oneHundred)
            .ignoresSafeArea()
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .background(AppStyle.Colors.spaceBlack)
        .navigationTitle(schoolName)
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                BackButtonView()
                    .onTapGesture {
                        presentation.wrappedValue.dismiss()
                    }
            }
        }
    }

    var topSection: some View {
        SchoolTopContainerView(school: school)
    }
    
    var statisticsView: some View {
        StatisticsView(school: school)
    }
    
    var elegibilityView: some View {
        ElegibilityView(school: school)
    }
}

struct SchoolDetail_Previews: PreviewProvider {
    @State static var school: School = School()
    static var previews: some View {
        SchoolDetailView(school: school)
    }
}
