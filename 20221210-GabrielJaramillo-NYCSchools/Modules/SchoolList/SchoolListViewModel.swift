//
//  SchoolListViewModel.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/11/22.
//

import Foundation

@MainActor
class SchoolListViewModel: ObservableObject {
    
    @Published var backgroundImage: BackgroundImage = BackgroundImage()
    @Published var searchTarget: String = ""
    @Published var showSearchBar: Bool = false
    let networkProvider: NetworkProvider
    
    @Published var schools: [School] = []
    
    init(networkProvider: NetworkProvider = NetworkProvider()) {
        self.networkProvider = networkProvider
        fetchSchools()
    }
    
    func fetchSchools() {
        Task {
            let result = try await self.networkProvider.fetch(endPoint: .schools, model: [School].self)
            switch result {
            case .success(let schools):
                self.schools = schools
            case .failure(let err):
                print("something went wrong \(err)")
            }
        }
    }
}
