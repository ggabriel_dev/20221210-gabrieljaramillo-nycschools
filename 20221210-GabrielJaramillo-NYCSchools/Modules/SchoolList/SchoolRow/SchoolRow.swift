//
//  SchoolRow.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/11/22.
//

import SwiftUI

struct SchoolRow: View {
    @State var school: School
    
    var schoolCity: String {
        let name = school.name ?? ""
        return name + AppStyle.StringPrefixes.stateAbbreviation
    }
    
    var totalStudents: String {
        let students = school.totalStudents ?? AppStyle.StringPrefixes.zeroStudents
        return students + AppStyle.StringPrefixes.totalStudents
    }
    
    var schoolDescription: String {
        let description = school.description ?? ""
        return description.prefix(AppStyle.Sizes.hundred) + "..."
    }
    
    var body: some View {
        content
            .padding([.leading, .trailing],
                     AppStyle.Spacing.four)
            .background(AppStyle.Colors.black)
    }
    
    var content:  some View {
        VStack {
            HStack {
                icon
                VStack {
                    schoolNameView
                    schoolDescriptionView
                    schoolDetailsView
                }
            }
            .padding(.all, AppStyle.Spacing.four)
            .frame(maxWidth: .infinity, alignment: .leading)
            .foregroundColor(AppStyle.Colors.white)
            
            Divider()
                .frame(maxWidth: .infinity, maxHeight: AppStyle.Sizes.two)
                .overlay(AppStyle.Colors.white)
        }
    }
    
    var icon: some View {
        Image(AppStyle.Icons.schoolx64)
            .frame(width: AppStyle.Sizes.ninetySix,
                   height: AppStyle.Sizes.ninetySix)
            .cornerRadius(AppStyle.Sizes.eight)
    }
    
    var schoolNameView: some View {
        Text(school.name!)
            .font(.custom(AppStyle.Fonts.staatlichesRegular,
                          size: AppStyle.Sizes.twenty))
            .multilineTextAlignment(.leading)
            .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    var schoolDescriptionView: some View {
        Text(schoolDescription)
            .font(.custom(AppStyle.Fonts.staatlichesRegular,
                          size: AppStyle.Sizes.twelve))
            .frame(maxWidth: .infinity, alignment: .leading)
            .multilineTextAlignment(.leading)
            .padding([.trailing],
                     AppStyle.Spacing.eight)
    }
    
    var schoolDetailsView: some View {
        HStack {
            Image(AppStyle.Icons.locationv2x16)
                .resizable()
                .renderingMode(.template)
                .frame(width: 20, height: 20, alignment: .leading)

            Text(schoolCity)
                .fontWeight(.semibold)
                .multilineTextAlignment(.leading)

            Image(AppStyle.Icons.studentx16)
                .resizable()
                .renderingMode(.template)
                .frame(width: 20, height: 20, alignment: .leading)

            Text(totalStudents)
                .fontWeight(.semibold)
                .multilineTextAlignment(.leading)
        }
        .font(.custom(AppStyle.Fonts.staatlichesRegular,
                      size: AppStyle.Sizes.twelve))
    }
}


struct SchoolRow_Previews: PreviewProvider {
    @State static var school: School = School(id: UUID(),
                                              name: "School Name",
                                              city: "School City",
                                              description: "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum... ")
    static var previews: some View {
        SchoolRow(school: school)
            .previewLayout(.fixed(width: 400, height: 150))
    }
}
