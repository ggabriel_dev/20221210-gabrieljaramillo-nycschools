//
//  SchoolList.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/11/22.
//

import SwiftUI

struct SchoolList: View {
    @Environment(\.presentationMode) var presentation
    @ObservedObject private var viewModel: SchoolListViewModel = SchoolListViewModel()
    
    var body: some View {
        NavigationView {
            content
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    
    var content: some View {
        ZStack {
            Color.black
                .ignoresSafeArea()
                .opacity(AppStyle.OpacityLevels.pointSeven)
                .zIndex(AppStyle.ZIndexes.zero)
            
            topContent
                .zIndex(AppStyle.ZIndexes.one)
            
            schoolScrollList
                .zIndex(AppStyle.ZIndexes.two)
        }
        .background(
            viewModel.backgroundImage
        )
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationTitle(AppStyle.SchoolList.Titles.navigationTitle)
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                backButtonIcon
            }
            ToolbarItem(placement: .navigationBarTrailing) {
                NavigationLink(destination:Text("Hello saved School")) {
                    savedSchoolsButton
                }
            }
        }
    }
    
    var topContent: some View {
        VStack {
            Text(AppStyle.SchoolList.Titles.mainTitle)
                .font(.custom(AppStyle.Fonts.alfaSlabOneRegular,
                              size: AppStyle.Sizes.twentyeight))
                .padding([.leading, .trailing], AppStyle.Sizes.sixteen)
                .padding([.top], AppStyle.Sizes.eighteen)
            
            Text(AppStyle.SchoolList.Titles.description)
                .font(.custom(AppStyle.Fonts.pontanoSans,
                              size: AppStyle.Sizes.sixteen))
                .fontWeight(.bold)
                .padding([.leading, .trailing],
                         AppStyle.Sizes.sixteen)
                .padding(.top,
                         AppStyle.Spacing.pointFive)
        }
        .foregroundColor(AppStyle.Colors.white)
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
    }
    
    var schoolScrollList: some View {
        ScrollView {
            //VStack that holds the top spacer with the List in ForEach, with NO Spacing between elements
            LazyVStack(spacing: .zero) {
                //HStack that will hold the Spacer and give capability to perform the Gradient
                HStack {
                    Spacer()
                        .frame(height: AppStyle.Sizes.treefifty)
                        .background(
                            LinearGradient(gradient:
                                            Gradient(colors: AppStyle.Colors.listBlackGradient),
                                           startPoint: .top,
                                           endPoint: .bottom)
                        )
                }
                // ForEach that will create cells for every school
                ForEach(viewModel.schools) { school in
                    NavigationLink(destination: destinationView(school: school)) {
                        SchoolRow(school: school)
                    }
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .foregroundColor(AppStyle.Colors.white)
        .searchable(text: $viewModel.searchTarget)
    }
    
    var savedSchoolsButton: some View {
        HStack {

            Image(AppStyle.Icons.starx32)
                .resizable()
                .frame(width: AppStyle.Sizes.twentytwo,
                       height: AppStyle.Sizes.twentytwo)

            Text(AppStyle.SchoolList.Navigation.savedSchoolsTitle)
                .font(.custom(AppStyle.Fonts.staatlichesRegular,
                              size: AppStyle.Sizes.sixteen))
                .foregroundColor(AppStyle.Colors.white)

        }
        .padding([.leading, .trailing], AppStyle.Sizes.four)
        .padding([.top, .bottom], AppStyle.Sizes.four)
        .background(
            RoundedRectangle(cornerRadius: AppStyle.Sizes.eight)
                .stroke(AppStyle.Colors.white,
                        lineWidth: AppStyle.Sizes.one)
                .foregroundColor(.clear)
        )
    }

    var backButtonIcon: some View {
        BackButtonView()
        .onTapGesture {
            presentation.wrappedValue.dismiss()
        }
    }
}

extension SchoolList {
    @ViewBuilder
    func destinationView(school: School) -> some View {
        SchoolDetailView(school: school)
    }
}

struct SchoolList_Previews: PreviewProvider {
    static var previews: some View {
        SchoolList()
            .previewInterfaceOrientation(.portrait)
    }
}
