//
//  AppStyle.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/10/22.
//

import SwiftUI

public struct AppStyle {
    
    struct OpacityLevels {
        static let pointSeven: Double = 0.1
        static let pointFour: Double = 0.4
    }
    
    struct Timming {
        static let halfSecond: Double = 0.5
    }
    
    struct Tags {
        static let zero: Int = 0
        static let one: Int = 0
        static let two: Int = 0
        static let three: Int = 0
        static let four: Int = 0
    }
    
    struct ZIndexes {
        static let zero: Double = 0
        static let one: Double = 0
        static let two: Double = 0
        static let three: Double = 0
        static let four: Double = 0
    }
    
    struct StringPrefixes {
        static let stateAbbreviation: String = ", NY"
        static let zeroStudents: String = "0"
        static let totalStudents: String = " total students."
        static let notAvailable: String = "Not Available"
    }
    
    struct Icons {
        static let starIconx32 = "starx32"
        static let starIconx64 = "starx64"
        static let addressx32 = "addressx32"
        static let addressx64 = "addressx64"
        static let awardx32 = "awardx32"
        static let award64 = "award64"
        static let backx32 = "backx32"
        static let backx64 = "backx64"
        static let bikex32 = "bikex32"
        static let bikex64 = "bikex64"
        static let busx32 = "busx32"
        static let busx64 = "busx64"
        static let carx32 = "carx32"
        static let carx64 = "carx64"
        static let emailx32 = "emailx32"
        static let emailx64 = "emailx64"
        static let extracurricularx16 = "extracurricularx16"
        static let checkmarkx16 = "checkmarkx16"
        static let websitex16 = "websitex16"
        static let chartx16 = "chartx16"
        static let bookx16 = "bookx16"
        static let emailx16 = "emailx16"
        static let addressx16 = "addressx16"
        static let bus2x16 = "bus2x16"
        static let busx16 = "busx16"
        static let subwayx16 = "subwayx16"
        static let studentx16 = "studentx16"
        static let extracurricularx32 = "extracurricularx32"
        static let extracurricularx64 = "extracurricularx64"
        static let faxx32 = "faxx64"
        static let faxx64 = "faxx64"
        static let fax2x16 = "fax2x16"
        static let fax2x32 = "fax2x32"
        static let locationv2x16 = "locationv2x16"
        static let locationx32 = "locationx32"
        static let locationx64 = "locationx64"
        static let medalx32 = "medalx32"
        static let medalx64 = "medalx64"
        static let motorbikex32 = "motorbikex32"
        static let motorbikex64 = "motorbikex64"
        static let phonex32 = "phonex32"
        static let phonex64 = "phonex64"
        static let sportsx32 = "sportsx32"
        static let sportsx64 = "sportsx64"
        static let starEmptyx32 = "starEmptyx32"
        static let starEmptyx64 = "starEmptyx64"
        static let starx32 = "starx32"
        static let starx64 = "starx64"
        static let subwayx32 = "subwayx32"
        static let subwayx64 = "subwayx64"
        static let websitex32 = "websitex32"
        static let websitex64 = "websitex32"
        static let schoolx32 = "schoolx32"
        static let schoolx64 = "schoolx64"
        static let schoolx128 = "schoolx128"
        static let schoolx256 = "schoolx256"
        static let malestdnx32 = "malestdnx32"
        static let malestudnx64 = "malestdnx64"
        static let femstdnx32 = "femstdnx32"
        static let femstdnx64 = "femstdnx64"
    }
    
    struct Colors {
        static let listBlackGradient: [Color] = [.clear,.clear,.clear,.clear,.black ]
        static let clear: Color = .clear
        static let white: Color = .white
        static let red: Color = .red
        static let black: Color = .black
        static let teal: Color = .teal
        static let spaceBlack: Color = Color(uiColor: UIColor(red: 23/255, green: 23/255, blue: 25/255, alpha: 1))
        static let blackBackground: Color = Color(.sRGB, red: 0, green: 0, blue: 0, opacity: 1)
        static let grayBackground: Color = Color(.sRGB, red: 28, green: 28, blue: 28, opacity: 1)
        static let lightGrayBackground: Color = Color(.sRGB, red: 42, green: 42, blue: 42, opacity: 1)
        static let lightGrayText: Color = Color(.sRGB, red: 179, green: 179, blue: 179, opacity: 1)
    }
    
    struct Images {
        static let defaultImageName: String = "ny-a"
        static let backgroundImages: [String] = [
            "ny-a","ny-b","ny-c","ny-d","ny-e","ny-f",
            "ny-g","ny-h","ny-i","ny-j","ny-k","ny-l",
            "ny-m","ny-n","ny-o","ny-p","ny-q","ny-r",
            "ny-s","ny-t","ny-u","ny-w","ny-x"
        ]
    }
    
    struct Fonts {
        static let alfaSlabOneRegular = "AlfaSlabOne-Regular"
        static let pontanoSans = "PontanoSans-Regular"
        static let staatlichesRegular = "Staatliches-Regular"
    }
    
    struct Sizes {
        static let one: CGFloat = 1.0
        static let two: CGFloat = 2.0
        static let four: CGFloat = 4.0
        static let six: CGFloat = 6.0
        static let eight: CGFloat = 8.0
        static let ten: CGFloat = 10.0
        static let twelve: CGFloat = 12.0
        static let fourteen: CGFloat = 14.0
        static let sixteen: CGFloat = 16.0
        static let eighteen: CGFloat = 18.0
        static let twenty: CGFloat = 20.0
        static let twentytwo: CGFloat = 22.0
        static let twentyfour: CGFloat = 24.0
        static let twentyeight: CGFloat = 28.0
        static let thirthy: CGFloat = 30.0
        static let thirtytwo: CGFloat = 32.0
        static let fourthy: CGFloat = 40.0
        static let fourthyeight: CGFloat = 48.0
        static let sixtyfour: CGFloat = 64.0
        static let eighty: CGFloat = 80.0
        static let ninetySix: CGFloat = 96.0
        static let hundred: Int = 100
        static let oneHundred: CGFloat = 100.0
        static let oneFifty: CGFloat = 150.0
        static let treefifty: CGFloat = 350.0
    }
    
    struct Spacing {
        static let pointFive: CGFloat = 0.5
        static let one: CGFloat = 1.0
        static let two: CGFloat = 2.0
        static let four: CGFloat = 4.0
        static let six: CGFloat = 6.0
        static let eight: CGFloat = 8.0
        static let ten: CGFloat = 10.0
        static let twelve: CGFloat = 12.0
        static let sixteen: CGFloat = 16.0
        static let twenty: CGFloat = 20.0
        static let thirthy: CGFloat = 30.0
        static let thirtytwo: CGFloat = 32.0
        static let fourthy: CGFloat = 40.0
        static let fourthyeight: CGFloat = 48.0
        static let sixtyfour: CGFloat = 64.0
        static let eighty: CGFloat = 80.0
        static let oneFifty: CGFloat = 150.0
    }
    
    struct Home {
        static let backgroundTimming: Double = 5.0
        static let iconTimming: Double = 0.5
        
        struct Background {
            static let color: Color = .black
            static let opacity: Double = 0.70
        }
        
        struct Images {
            static let hatx64: String = "school-hatx64"
            static let hat256: String = "school-hatx256"
        }
        
        struct Titles {
            static let welcome: String = "Welcome"
            static let welcomeSize: Double = 48
            static let to: String = "to"
            static let toSize: Double = 18
            static let newYorkSchools: String = "New York Schools"
            static let newYorkSchoolsSize: Double = 30
            static let startSearching: String = "Star Searching"
        }
    }
    
    struct SchoolList {
        struct Titles {
            static let mainTitle: String = "Directory of Department of Education Schools in 2017"
            static let navigationTitle: String = "New York Schools"
            static let description: String = "Information gathered to provide information to all New York City students, in specific, this will help you to know more about your current schools in districts, what extracurricular activities they provide, studen populations, location information and which transport you can use based on your location and many more."
        }
        
        struct Navigation {
            static let backButtonTitle: String = "Back"
            static let savedSchoolsTitle: String = "Saved"
        }
    }
    
    struct SchoolDetail {
        struct SchoolTopContainerView {
            static let directionsTitle: String = "Directions"
            static let busRoutes: String = "Bus Routes: "
            static let informationTitle: String = "Information"
            static let transportation: String = "Transportation"
        }
        struct Statistics {
            static let title: String = "Statistics"
            static let description: String = "Interesting Facts about current School."
        }
        
        struct Elegibility {
            static let title: String = "Elegibility"
            static let description: String = "Rules for School eleibility:"
        }
    }
}
