//
//  SchoolItem.swift
//  20221210-GabrielJaramillo-NYCSchools
//
//  Created by Luis Jaramillo Perez on 12/11/22.
//

import Foundation

struct School: Codable, Identifiable {
    var id: UUID = UUID()
    var schoolId: String?
    var name: String?
    var campusName: String?
    var addressLine: String?
    var city: String?
    var zipCode: String?
    var state: String?
    var description: String?
    var grades: String?
    var finalGrades: String?
    var totalStudents: String?
    var email: String?
    var website: String?
    var phone: String?
    var fax: String?
    var buses: String?
    var subway: String?
    var collegeCareerRate: String?
    var attendanceRate: String?
    var studentVariety: String?
    var studentSafety: String?
    var elegibilityA: String?
    var elegibilityB: String?
    var directionsA: String?
    var schoolSports: String?
    var extracurricularActivities: String?
    var additionalInfo: String?
    var academicOpportunitiesA: String?
    var academicOpportunitiesB: String?
    var academicOpportunitiesC: String?
    
    
    enum CodingKeys: String, CodingKey {
        case schoolId = "dbn"
        case name = "school_name"
        case campusName = "campus_name"
        case addressLine = "primary_address_line_1"
        case city = "city"
        case zipCode = "zip"
        case state = "state_code"
        case description = "overview_paragraph"
        case grades = "grades2018"
        case finalGrades = "finalgrades"
        case totalStudents = "total_students"
        case email = "school_email"
        case website = "website"
        case phone = "phone_number"
        case fax = "fax_number"
        case buses = "bus"
        case subway = "subway"
        case collegeCareerRate = "college_career_rate"
        case attendanceRate = "attendance_rate"
        case studentVariety = "pct_stu_enough_variety"
        case studentSafety = "pct_stu_safe"
        case elegibilityA = "eligibility1"
        case elegibilityB = "eligibility2"
        case directionsA = "directions1"
        case schoolSports = "school_sports"
        case extracurricularActivities = "extracurricular_activities"
        case additionalInfo = "addtl_info1"
        case academicOpportunitiesA = "academicopportunities1"
        case academicOpportunitiesB = "academicopportunities2"
        case academicOpportunitiesC = "academicopportunities3"
    }
    
    func buildBuses() -> String {
        buses ?? AppStyle.StringPrefixes.notAvailable
    }
    
    func buildSubway() -> String {
        subway ?? AppStyle.StringPrefixes.notAvailable
    }
    
    func buildStatistics() -> [String] {
        var statistics: [String] = []
        statistics.append("Grades: " + (grades ?? "not Available"))
        statistics.append("Final Grades: " + (finalGrades ?? "Not Available"))
        statistics.append("# Students: " + (totalStudents ?? "Not available."))
        statistics.append("College career rate: " + (collegeCareerRate ?? "Not Available"))
        statistics.append("Attendance rate: " + (attendanceRate ?? "Not Available"))
        statistics.append("Student Variety %: " + (studentVariety ?? "NotAvailable"))
        statistics.append("Student Safety %: " + (studentSafety ?? "Not Available"))
        return statistics
    }
    
    func buildElegibility() -> String {
        var result: String = ""
        if let value = elegibilityA {
            result.append(value)
        }
        if let value = elegibilityB {
            result.append(value)
        }
        if result.isEmpty {
            result = AppStyle.StringPrefixes.notAvailable
        }
        return result
    }
    
    func buildSchoolAddress() -> String {
        var result = ""
        
        if let value = addressLine {
            result.append(value + " ")
        }
        
        if let value = city {
            result.append(value + " ")
        }
        
        if let value  = state {
            result.append(value + " ")
        }
        
        if let value  = zipCode {
            result.append(value + " ")
        }
        
        if !result.isEmpty {
            result.append(", US.")
        }
        
        return result
    }
    
    func buildWebsite() -> String {
        website ?? AppStyle.StringPrefixes.notAvailable
    }
    
    func buildEmail() -> String {
        email ?? AppStyle.StringPrefixes.notAvailable
    }
    
    func buildPhone() -> String {
        phone ?? AppStyle.StringPrefixes.notAvailable
    }
    
    func buildFax() -> String {
        fax ?? AppStyle.StringPrefixes.notAvailable
    }
    
    func buildDirections() -> String {
        directionsA ?? AppStyle.StringPrefixes.notAvailable
    }
    
}
