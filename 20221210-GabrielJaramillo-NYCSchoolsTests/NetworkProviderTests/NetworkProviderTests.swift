//
//  NetworkProviderTests.swift
//  20221210-GabrielJaramillo-NYCSchoolsTests
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import XCTest
@testable import _0221210_GabrielJaramillo_NYCSchools

class NetworkProviderTests: XCTestCase {
    
    private var systemUnderTest: NetworkProviderMock!
    private var networkProvider: NetworkProvider!
    private var viewModel: SchoolListViewModel!
    
    @MainActor override func setUp() {
        super.setUp()
        systemUnderTest = NetworkProviderMock()
        networkProvider = NetworkProvider(session: systemUnderTest)
        viewModel = SchoolListViewModel(networkProvider: networkProvider)

    }
    /*
     * MARK: This will Test the Mocking Object by getting the school array result
     *       that we pass to The Mocking Object instead of using the Real HTTP Callback
     */
     
    func testGetSchools() async throws {
        // Given
        let school = [School(id: UUID(), name: "School name")]
        let expectedSchoolCount = 1
        systemUnderTest.result = try .success(JSONEncoder().encode(school))
        networkProvider = NetworkProvider(session: systemUnderTest)
        var schoolCount = await viewModel.schools.count
        
        // When
        XCTAssertEqual(schoolCount, 0, "Error - School Datasource should be Empty.")
        await viewModel.fetchSchools()
        
        //Then
        let schoolsAreEmpty = await viewModel.schools.isEmpty
        schoolCount = await viewModel.schools.count
        XCTAssertFalse(schoolsAreEmpty, "Error - Schools shouldn't be empty.")
        XCTAssertEqual(schoolCount, expectedSchoolCount, "Error - Schools should contain at least one element.")
    }
}
