//
//  NetworkProviderMock.swift
//  20221210-GabrielJaramillo-NYCSchoolsTests
//
//  Created by Luis Jaramillo Perez on 12/12/22.
//

import Foundation
@testable import _0221210_GabrielJaramillo_NYCSchools

// MARK: Mock Class that Implements URLSessionProtocol
class NetworkProviderMock: URLSessionProtocol {
    var result = Result<Data, Error>.success(Data())
    
    func data(from url: URL, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        
        let success = try result.get()
        
        return (success, URLResponse())
    }
}
